Molecular dynamics example settings
===================================

Simulation settings
-------------------
task = md-nve
steps = 900
restart = ../run-100/pyretis.restart

Engine settings
---------------
class = velocityverlet
timestep = 0.002

System settings
---------------
units = lj

Forcefield settings
-------------------
description = Lennard Jones test

Potential
---------
class = PairLennardJonesCutnp
shift = True
parameter 0 = {'sigma': 1.0, 'epsilon': 1.0, 'rcut': 2.5}

Output
------
prefix = md-100-1000-
backup = overwrite
energy-file = 1
order-file = 10
cross-file = 1
trajectory-file = 10
restart-file = 10
