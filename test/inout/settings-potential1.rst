Potential
---------
class = PairLennardJonesCut
shift = False
mixing = geometric
parameter 0 = {'sigma': 1.0, 'epsilon': 1.0, 'rcut': 2.5}
parameter 1 = {'sigma': 2.0, 'epsilon': 2.0, 'rcut': 2.5}
