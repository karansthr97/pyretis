Particles
---------
position = {
    'generate': 'fcc',
    'repeat': [3, 3, 3],
    'lcon': 1.0
}
type = [0, 1]

System
------
units = lj
