Particles
---------
position = {
    'generate': 'fcc',
    'repeat': [6, 6, 6],
    'lcon': 1.0
}

System
------
units = lj
