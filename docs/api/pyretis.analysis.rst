
.. _api-analysis:

pyretis.analysis package
========================

.. automodule:: pyretis.analysis
    :members:
    :undoc-members:
    :show-inheritance:


List of submodules
------------------

* :ref:`pyretis.analysis.analysis <api-analysis-analysis>`
* :ref:`pyretis.analysis.energy_analysis <api-analysis-energy_analysis>`
* :ref:`pyretis.analysis.flux_analysis <api-analysis-flux_analysis>`
* :ref:`pyretis.analysis.histogram <api-analysis-histogram>`
* :ref:`pyretis.analysis.order_analysis <api-analysis-order_analysis>`
* :ref:`pyretis.analysis.path_analysis <api-analysis-path_analysis>`

.. _api-analysis-analysis:

pyretis.analysis.analysis module
--------------------------------

.. automodule:: pyretis.analysis.analysis
    :members:
    :undoc-members:
    :show-inheritance:

.. _api-analysis-energy_analysis:

pyretis.analysis.energy_analysis module
---------------------------------------

.. automodule:: pyretis.analysis.energy_analysis
    :members:
    :undoc-members:
    :show-inheritance:

.. _api-analysis-flux_analysis:

pyretis.analysis.flux_analysis module
-------------------------------------

.. automodule:: pyretis.analysis.flux_analysis
    :members:
    :undoc-members:
    :show-inheritance:

.. _api-analysis-histogram:

pyretis.analysis.histogram module
---------------------------------

.. automodule:: pyretis.analysis.histogram
    :members:
    :undoc-members:
    :show-inheritance:

.. _api-analysis-order_analysis:

pyretis.analysis.order_analysis module
--------------------------------------

.. automodule:: pyretis.analysis.order_analysis
    :members:
    :undoc-members:
    :show-inheritance:

.. _api-analysis-path_analysis:

pyretis.analysis.path_analysis module
-------------------------------------

.. automodule:: pyretis.analysis.path_analysis
    :members:
    :undoc-members:
    :show-inheritance:
